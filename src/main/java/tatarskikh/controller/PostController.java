package tatarskikh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PostController {

    @RequestMapping(value = "/posts" , method = RequestMethod.GET)
    public String getPosts(){
        return "posts";
    }

    @RequestMapping(value = "/posts/add" , method = RequestMethod.GET)
    public String getAddPost(){
        return "post-add";
    }

    @RequestMapping(value = "/posts/add" , method = RequestMethod.POST)
    public String addPost(){
        return "redirect:/posts";
    }

}
