package tatarskikh.form;

public class ReportForm {

    private String text;

    public ReportForm() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
